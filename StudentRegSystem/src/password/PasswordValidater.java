package password;

public class PasswordValidater {
	public static void main(String[] args) 
	{
		System.out.print(checkDigits("12asdfghjk"));
	}
	
	public static boolean checkPasswordLength(String password) {
		 if (password.length() < 8) {
			 return false;
		 }else {
			 return true;
		 }
	}
	
	public static boolean checkDigits(String password) {
		int digitCount = 0;
		 for (int i = 0; i < password.length(); i++) {
			 char ch = password.charAt(i);
			  if (is_Digit(ch)) { 
				  digitCount ++;
			  }
		 }
		 if(digitCount < 2) {
			return false;
		 }else {
			 return true;
		 }

	}
	
    public static boolean is_Digit(char ch) {
        return (ch >= '0' && ch <= '9');
    }
	
}
