package password;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class PasswordValidaterTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCheckPasswordLength() {
		boolean passward = PasswordValidater.checkPasswordLength("12asdfgh");
		assertTrue("The password length not correct", passward == true);
	}
	
	@Test
	public void testCheckPasswordLengthBoundaryIn() {
		boolean passward = PasswordValidater.checkPasswordLength("12asdfgh");
		assertTrue("The password length not correct", passward == true);
	}
	
	@Test
	public void testCheckPasswordLengthBoundaryOut() {
		boolean passward = PasswordValidater.checkPasswordLength("12asdfg");
		assertTrue("The password length not correct", passward == false);
	}

	@Test
	public void testCheckPasswordLengthException() {
		boolean passward = PasswordValidater.checkPasswordLength(" ");
		assertTrue("The password length not correct", passward == false);
	}
	
	@Test
	public void testCheckDigits() {
		boolean passward = PasswordValidater.checkDigits("12qsdfgh");
		assertTrue("The password doesn't have enough digits", passward == true);
	}


	@Test
	public void testCheckDigitsBoundaryIn() {
		boolean passward = PasswordValidater.checkDigits("12qasdfg");
		assertTrue("The password doesn't have enough digits", passward == true);
	}
	
	@Test
	public void testCheckDigitsBoundarOut() {
		boolean passward = PasswordValidater.checkDigits("1qasdfgh");
		assertTrue("The password doesn't have enough digits", passward == false);
	}
	
	@Test
	public void testCheckDigitsException() {
		boolean passward = PasswordValidater.checkDigits("kqasdfgh");
		assertTrue("The password doesn't have enough digits", passward == false);
	}

}
